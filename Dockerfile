FROM ruby:2.5.3
MAINTAINER FME

RUN export DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install  -qq -y  curl
RUN apt-get -qq -y install sqlite libsqlite3-dev libxml2-dev libxslt-dev tzdata libcurl3 openssh-server openssh-client vim git zip unzip sendmail

EXPOSE 3000
